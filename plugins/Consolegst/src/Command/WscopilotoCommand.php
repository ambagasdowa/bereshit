<?php
// namespace App\Command;
namespace Consolegst\Command;
// namespace App\Controller;

use Cake\Core\Configure;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;

use Cake\I18n\Date;
use Cake\I18n\Time;


use Cake\ORM\TableRegistry;
use Cake\ORM\Entity;
use Cake\Datasource\EntityInterface;

use Cake\Database\Expression\IdentifierExpression;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

use SoapClient;

use Cake\Http\Client;
use Cake\Http\Client\FormData;

class WscopilotoCommand extends Command {

  public function initialize() {
      parent::initialize();
      // $this->loadModel('Users');
      $this->loadModel('Consolegst.WsTblGstCopilotoHistoricoUnidades');
  }


  protected function buildOptionParser(ConsoleOptionParser $parser) {
      $parser
          ->addArgument('name', [
              'help' => 'What is your name'
          ])
          ->addOption('get', [
              'help' => 'get the row only',
              'boolean' => true
          ])
          ->addOption('save', [
              'help' => 'save the ws dataset',
              'boolean' => true
          ])
          ->addOption('yell', [
              'help' => 'Shout the name',
              'boolean' => true
          ])
          ;

      return $parser;
  }

  protected function _ws_hist_unit () {

    ini_set('memory_limit', '2048M');
    date_default_timezone_set('America/Mexico_City');
    // debug(date('Y-m-d H:i:s'));

    $fecha = gmdate('Y-m-d H:i:s');
    // debug($fecha);
    $date_ini = new Time($fecha);
    // $date_ini->modify('-30 minutes');
    $date_ini->modify('-125 seconds');
    // $date_ini->sub(new DateInterval('PT300S'));
    $fechaInicial = $date_ini->format('Y-m-d\TH:i:s');

    $date_end = new Time($fecha);
    $date_end->modify('-55 seconds');
    $fechaFinal = $date_end->format('Y-m-d\TH:i:s');

    // debug($fechaInicial);
    // debug($fechaFinal);
//NOTE WORKING EXAMPLE
    $client = new \SoapClient("https://gst.webcopiloto.com/ws/wgst/apicopiloto/ApiUnidades.asmx?wsdl");

    $params = [
                "token" => 'Gst2019C0p1l0tO',
                "fechaInicial" => $fechaInicial,
                "fechaFinal" => $fechaFinal,
              ];

    $response = $client->__soapCall("historicoUnidades", array($params));
    $response = $response->historicoUnidadesResult->Eventos->MovilHistorico;

    // NOTE for save in a datastore set one minute and save -1 minute store
    // 1 minute is aprox 670 records and the limit is 1000 records per transaction
    // return $historicoUnidadesws;
    return $response;
  }


  public function execute(Arguments $args, ConsoleIo $io) {
      $name = $args->getArgument('name');
      // $io->out(print_r($args->getOption('yell')));
      if ($args->getOption('yell')) {
          $name = mb_strtoupper($name);
      }
      $ws = TableRegistry::getTableLocator()->get('Consolegst.WsTblGstCopilotoHistoricoUnidades');

      $data = json_decode(json_encode($this->_ws_hist_unit()), TRUE);
      // debug($data);
      // exit();
      $entities = $ws->newEntities($data);
      // debug($ws);
      // exit();
      $entities = $ws->patchEntities($entities,$data,[
                                              'fields' =>
                                                          [
                                                             'alias'
                                                            ,'nombre'
                                                            ,'idMovil'
                                                            ,'latitud'
                                                            ,'longitud'
                                                            ,'lugar'
                                                            ,'placas'
                                                            ,'fechahorautc'
                                                            ,'tipo'
                                                          ],
                                                          // [
                                                          //   'validate' => false
                                                          // ]
                                              ]);
            // debug($entities);
            // exit();
            if($ws->saveMany($entities)) {
              $io->out(print_r('executing', true));
              $io->out(print_r('saving records', true));
            } else {
              var_dump($entities);
              $io->out(print_r('error saving records ', true));
            }
             $io->out(print_r('executing', true));
        }

}

?>
