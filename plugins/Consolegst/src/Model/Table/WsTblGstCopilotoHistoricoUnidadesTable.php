<?php
namespace Consolegst\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WsTblGstCopilotoHistoricoUnidades Model
 *
 * @method \Consolegst\Model\Entity\WsTblGstCopilotoHistoricoUnidade get($primaryKey, $options = [])
 * @method \Consolegst\Model\Entity\WsTblGstCopilotoHistoricoUnidade newEntity($data = null, array $options = [])
 * @method \Consolegst\Model\Entity\WsTblGstCopilotoHistoricoUnidade[] newEntities(array $data, array $options = [])
 * @method \Consolegst\Model\Entity\WsTblGstCopilotoHistoricoUnidade|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Consolegst\Model\Entity\WsTblGstCopilotoHistoricoUnidade saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Consolegst\Model\Entity\WsTblGstCopilotoHistoricoUnidade patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Consolegst\Model\Entity\WsTblGstCopilotoHistoricoUnidade[] patchEntities($entities, array $data, array $options = [])
 * @method \Consolegst\Model\Entity\WsTblGstCopilotoHistoricoUnidade findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class WsTblGstCopilotoHistoricoUnidadesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ws_tbl_gst_copiloto_historico_unidades');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            // ->requirePresence('id', 'create')
            ->allowEmptyString('id', false);

        $validator
            ->scalar('alias')
            ->maxLength('alias', 55)
            ->allowEmptyString('alias');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 255)
            ->allowEmptyString('nombre');

        $validator
            ->scalar('idMovil')
            ->maxLength('idMovil', 255)
            ->allowEmptyString('idMovil');

        $validator
            ->decimal('latitud')
            ->allowEmptyString('latitud');

        $validator
            ->decimal('longitud')
            ->allowEmptyString('longitud');

        $validator
            ->scalar('lugar')
            ->maxLength('lugar', 255)
            ->allowEmptyString('lugar');

        $validator
            ->scalar('placas')
            ->maxLength('placas', 255)
            ->allowEmptyString('placas');

        $validator
            ->scalar('fechahorautc')
            ->maxLength('fechahorautc', 255)
            ->allowEmptyString('fechahorautc');
        //
        // $validator
        //     ->dateTime('fechahorautc')
        //     ->allowEmptyDateTime('fechahorautc');

        $validator
            ->integer('tipo')
            ->allowEmptyString('tipo');

        $validator
            ->allowEmptyString('status');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'gst';
    }
}
