<?php
namespace Consolegst\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * WsTblGstCopilotoHistoricoUnidadesFixture
 */
class WsTblGstCopilotoHistoricoUnidadesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 19, 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'alias' => ['type' => 'string', 'fixed' => true, 'length' => 55, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null],
        'nombre' => ['type' => 'string', 'fixed' => true, 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null],
        'idMovil' => ['type' => 'biginteger', 'length' => 19, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'latitud' => ['type' => 'decimal', 'length' => 18, 'precision' => 0, 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'longitud' => ['type' => 'decimal', 'length' => 18, 'precision' => 0, 'null' => true, 'default' => null, 'comment' => null, 'unsigned' => null],
        'lugar' => ['type' => 'string', 'fixed' => true, 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null],
        'placas' => ['type' => 'string', 'fixed' => true, 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'Modern_Spanish_CI_AS', 'precision' => null, 'comment' => null],
        'fechahorautc' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'tipo' => ['type' => 'integer', 'length' => 10, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => 'getdate', 'precision' => null, 'comment' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'status' => ['type' => 'tinyinteger', 'length' => 3, 'null' => true, 'default' => '1', 'precision' => null, 'comment' => null, 'unsigned' => null],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'alias' => 'Lorem ipsum dolor sit amet',
                'nombre' => 'Lorem ipsum dolor sit amet',
                'idMovil' => 1,
                'latitud' => 1.5,
                'longitud' => 1.5,
                'lugar' => 'Lorem ipsum dolor sit amet',
                'placas' => 'Lorem ipsum dolor sit amet',
                'fechahorautc' => 1561595728,
                'tipo' => 1,
                'created' => 1561595728,
                'modified' => 1561595728,
                'status' => 1
            ],
        ];
        parent::init();
    }
}
